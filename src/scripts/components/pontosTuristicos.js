export class pontosTuristicos {
    constructor() {
        this.list = [];

        this.selectors();
        this.events();
    }

    selectors() {
        this.form = document.querySelector(".add-ponto-turistico-form");
        this.imageInput = document.querySelector(".add-image-input");
        this.titleInput = document.querySelector(".add-infos-title");
        this.descriptionInput = document.querySelector(".add-infos-description");
        this.listaPontosTuristicos = document.querySelector(".pontos-turisticos-container");
    }

    events() {
        this.form.addEventListener("submit", this.addPontoTuristico.bind(this));
        this.imageInput.addEventListener("change", this.uploadImage.bind(this));
    }

    uploadImage() {
        const fileReader = new FileReader();
        fileReader.onload = function () {
            sessionStorage.setItem("imagemURL", fileReader.result);
        };

        fileReader.readAsDataURL(this.imageInput.files[0]);
    }

    addPontoTuristico(event) {
        event.preventDefault();

        const imageContent = sessionStorage.getItem("imagemURL");
        const titleContent = event.target["input-add-title"].value;
        const descriptionContent = event.target["input-add-description"].value;

        if (titleContent != "" && descriptionContent != "") {
            const pontoTuristico = {
                image: imageContent,
                name: titleContent,
                description: descriptionContent,
            };

            this.list.push(pontoTuristico);

            this.renderPontoTuristico();
            this.resetInputs();
        }
    }

    renderPontoTuristico() {
        let pontoTuristicoStructure = "";

        this.list.forEach(function (pontoTuristico) {
            pontoTuristicoStructure += `
        <div class="ponto-turistico-card">
          <div class="ponto-turistico-image-wrapper">
            <img
              class="ponto-turistico-image"
              src=${pontoTuristico.image}
              alt="ponto turístico"
            />
          </div>
          <div class="ponto-turistico-infos-wrapper">
            <h3 class="ponto-turistico-title">${pontoTuristico.name}</h3>
            <p class="ponto-turistico-description">${pontoTuristico.description}
            </p>
          </div>
        </div>
        `;
        });

        this.listaPontosTuristicos.innerHTML = pontoTuristicoStructure;
    }

    resetInputs() {
        this.titleInput.value = "";
        this.descriptionInput.value = "";
    }
}
